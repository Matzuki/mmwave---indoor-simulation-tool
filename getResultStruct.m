function [results] = getResultStruct()
    results = struct('antTx',{},'antRx',{},'antIF',{},'environment',{},'paths',{},'rsig',{},'toa',{},'pathlength',{},'refl_coef',{},'txAngle',{},'rxAngle',{},'refAngle',{},'p_loss',{},'delay_samp',{},'imp_resp',{},'interference',{},'int_resp',{},'SIR',{},'sig',{},'SNR',{});
end

