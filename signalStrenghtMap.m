function signalStrenghtMap(antTx,environment)
% close all;
fc  = 60e9;
c = physconst('lightspeed');
lambda = c/fc;
txpos = antTx.pos;
txdir = antTx.direction;
environment(:,5) = rad2deg(environment(:,5));
roomdim = [-environment(1,3)/2, environment(1,3)/2, -environment(1,4)/2, environment(1,4)/2]; %[xmin,xmax,ymin,ymax]
%Homegrid
% stepwidth = 0.001;
%Notebookgrid;
stepwidth = 0.005;
[X,Y] = meshgrid(roomdim(1):stepwidth:roomdim(2),roomdim(3):stepwidth:roomdim(4));
Z = zeros(size(X));

[az,el,r] = cart2sph(X,Y,Z);
ant = cAntenna('model',txpos,txdir,'enable_Beamforming',true);
w = antTx.weights;

angle = linspace(-180,180,size(az,1));
pat = antTx.antObj.pattern(60e9,wrapTo180(angle),0,'Type','efield','CoordinateSystem','rectangular','Normalize',false,'Weights',w);
% figure('Renderer', 'painters', 'Position', [10 10 800 600])
figure;
% antTx.antObj.pattern(60e9,wrapTo180(angle),0,'Type','efield','CoordinateSystem','rectangular','Normalize',false,'Weights',w);
% figure;
F = zeros(size(az));
angle = deg2rad(wrapTo180(angle+txdir));

%Cartesian
for row=1:size(X,1)
    for col = 1:size(X,2)
        tmp = abs(atan2(Y(row,col)-txpos(2),X(row,col)-txpos(1))-angle);
        [~,index] = min(tmp);
        F(row,col)=pat(index);
    end
end

%Spherical
% for row=1:size(az,1)
%     for col = 1:size(az,2)
%         tmp = abs(az(row,col)-angle);
%         [~,index] = min(tmp);
%         F(row,col)=pat(index);
%     end
% end

%Cartesian 
ploss = (lambda./(4*pi*sqrt((X-txpos(1)).^2+(Y-txpos(2)).^2))).^2;
ploss(isinf(ploss)) = 1;
F = ploss.*F;

F(sqrt((X-txpos(1)).^2+(Y-txpos(2)).^2)<0.1) = 0;
F(sqrt((X-txpos(1)).^2+(Y-txpos(2)).^2)<0.1) = max(F,[],'all');

% Spherical
% ploss = (lambda./(4*pi*r)).^2;
% ploss(isinf(ploss)) = 1;
% F = ploss.*F;
% F(r<0.1) = 0;
% F(r<0.1) = max(F,[],'all');
F = calcLitRegion(X,Y,F,txpos,environment);
F = F/max(F,[],'all');
F = mag2db(F);
F(isinf(F)) = -200;

% [boxtheta,~,boxr] = cart2sph([1,1],[-1,1],[0,0]);
% A = r>=boxr(1) & boxtheta(2)>=az&boxtheta(1)<=az;
% F(A) = -200;
% B = X>=1&Y<=1&Y>=-1;
% F(B) = -200;

surf(X,Y,F,'EdgeColor','none');

colormap(jet(20))
colorbar
hold on;
drawOrientedBox(environment(2:end,:))
% drawOrientedBox(obstacles);
view(2)
axis equal
set(gca,'visible','off')
% dumpFigure;
% figure;
% imagesc(unique(X),unique(Y),F)
% xlim([-2.5 2.5]);
% ylim([-2.5 2.5]);
% colormap(jet(20))
end