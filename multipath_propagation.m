function [ results ] = multipath_propagation( varargin )
%MULTIPATH_PROPAGATION 
%   Usage: 
%       rsig = mulitpath_propagation(sig, txpos, rxpos, array, txdir
%       rxdir,...)
%
%   Required Inputs:
%       sig:                Signal to be transmitted. Has to be a column vector
%       antTx:              Antenna Array Element of transmitters
%       antRx:              Antenna Array Element of receivers
%   Optional Inputs:
%       room:               Dimension of the 2D room [x,y]
%       obstacles:          Structur of the obstacles inside the room.
%       eps_wall:           Relative Permittivity of the walls
%       max_refl:           Number of reflections which are taken into account
%       fc:                 Carrier frequency of the transmitted signal
%       f_sample:           Samplefrequency
%       enable_Beamforming: Enables Beamforming at the transmitter
%
%   Outputs:
%       rsig:               Received Signal after multiple reflections
%              
%   TODO: Gain Werte anhand der Messwerte anpassen.         
%% Parse the inputs
    p=inputParser();
    p.addRequired('sig', @ismatrix);
    p.addRequired('antTx',@(x) isa(x,'cAntenna'));
    p.addRequired('antRx',@(x) isa(x,'cAntenna'));
    p.addRequired('f_sample', @isnumeric);
    p.addParameter('room', [8 ,6], @isvector);
    p.addParameter('obstacles', [], @ismatrix);
    p.addParameter('eps_wall', 3.24, @isnumeric);
    p.addParameter('max_refl', 1.0, @isnumeric);
    p.addParameter('fc', 60e9, @isnumeric);
    
    p.parse(varargin{:});
    cfg = p.Results();
%%
    % Create all system objects
    results = getResultStruct();
    if(numel(cfg.antTx)~=numel(cfg.antRx))
        error('Number of transmit and receive antennas has to be equal!');
    end
    if(size(cfg.sig,2)~=numel(cfg.antTx))
        error('Number of signals are not equivalent to the transceiver pars!');
    end
    % Allocate the signals and interference antennas for each tranceiver
    % par
    for ii=1:numel(cfg.antTx)
        results(ii).antTx = cfg.antTx(ii);
        results(ii).sig(1) = cfg.sig(ii);
        if(numel(cfg.antTx)>1)
            results(ii).antIF = cfg.antTx(1:end ~= ii);
            results(ii).sig(2:numel(cfg.antTx)) = cfg.sig(1:end ~= ii);
        end
    end
    %Calculate all Parameters for each tranceiver pair
    for ii=1:numel(cfg.antTx)
        environment = getEnvironment(cfg.room,cfg.eps_wall,cfg.obstacles);
        results(ii).environment = environment;
        results(ii).antRx = cfg.antRx(ii);
        % Calculate paths    
        results(ii).paths{1} = tracePaths(cfg.antTx(ii).pos, cfg.antRx(ii).pos, environment, cfg.max_refl);
        for i_IF=1:numel(results(ii).antIF)
            results(ii).paths{i_IF+1} = tracePaths(results(ii).antIF(i_IF).pos, cfg.antRx(ii).pos, environment, cfg.max_refl);
        end
        % Calculate all needed angle (relative angle to mainlobe,reflection
        % angle
        results(ii).txAngle{1} = relAngle(results(ii).paths{1},cfg.antTx(ii).direction,'TX');
        results(ii).rxAngle{1} = relAngle(results(ii).paths{1},cfg.antRx(ii).direction,'RX');
        %Calculate all interference angles
        for i_IF=1:numel(results(ii).antIF)
           results(ii).txAngle{i_IF+1} = relAngle(results(ii).paths{i_IF+1},results(ii).antIF(i_IF).direction,'TX');
           results(ii).rxAngle{i_IF+1} = relAngle(results(ii).paths{i_IF+1},cfg.antRx(ii).direction,'RX');
        end
        %Calculate loss, delay, reflection paramteter
        results(ii).delay_samp{1} = cell(1,numel(results(ii).antIF)+1);
        for numPair = 1:numel(results(ii).antIF)+1
            results(ii).refAngle{numPair} = rad2deg(getPathReflectionAngles(results(ii).paths{numPair}));
            results(ii).pathlength{numPair} = getPathLength(results(ii).paths{numPair});

            results(ii).toa{numPair} = results(ii).pathlength{numPair}/physconst('lightspeed');
            results(ii).refl_coef{numPair} = getReflectionCoeff(environment,results(ii).paths{numPair},results(ii).refAngle{numPair});

            results(ii).reflOrder{numPair} = sum(~isnan(results(ii).refAngle{numPair}),2);
            results(ii).maxRefl = cfg.max_refl;
            [results(ii).delay_samp{numPair}, results(ii).p_loss{numPair}] = losses_delay(results(ii).toa{numPair},results(ii).pathlength{numPair},cfg.f_sample);
        end
        %Calculate the Weights for the Path with the highest SNR
        [results(ii)] = getWeightsHighestSNR(results(ii));
    end
    % Assing the Interference Antennas to each tranceiver pair 
    for ii=1:numel(results)
        temp_results = results(1:end ~=ii);
        for i_IF=1:numel(temp_results)
            results(ii).antIF(i_IF) = temp_results(i_IF).antTx;
        end
    end
%         %% Calculate SIR for the secotors in 20� steps   
%         antInt = {results(1:numel(results) ~=ii).antTx};
%         [SIR,sectors] = getBestSector(results(ii).antTx,results(ii).antRx,antInt,cfg);
%         results(ii).SIR = [sectors,SIR];
%%
    for ii=1:numel(results)
        [results(ii).rsig,results(ii).imp_resp,results(ii).interference,results(ii).int_resp,results(ii).SNR] = getRSig(results(ii));
    end
end