%%
% Simulate a indoor multipath propagation with a model of a front antenna
% of the TP-Link Talon AD7200 60 GHz Router. The antenna is model pattern
% synthesis

% close all
numRefl = 2;
t = 0:1/10e9:100e-9; % Time Vector

%%
f_samp = 10e9;
% Position of TX and RX antenna
txpos = [-2,0];
rxpos = [2,1];
% Create a room with rectangular obstacles. [xmid,ymid,xlength,ylenght,rotation,epsilon]
obstacles = [1,1.5,1,0.5,deg2rad(30),2.1;...
    0,-1.5,2,0.5,0,3];
% obstacle = []; %Empty room
room = [8,5];

%Azimuth direction of zero direction of the antenna
txdir = 0;
rxdir = -130; 
%Antennaobjects of Tx and Rx;
antTx(1) = cAntenna('model',txpos,txdir,'enable_Beamforming',true);
antRx(1) = cAntenna('model',rxpos,rxdir,'enable_Beamforming',true);
%Signal to be transmitted
f_samp = 10e9;
t = 0:1/f_samp:100e-9; % Time Vector
t = t(1:end-1);
signal = cell(1,1);
signal{1} = zeros(length(t),size(antTx,2));
signal{1} = 2.9*(sin(2*pi*100e6*t))';
signal{1} = (sin(2*pi*100e6*t))';
% Raytracing Simulation
tic
multi_set = multipath_propagation(signal,antTx,antRx,f_samp,'room',room,'obstacles',obstacles,'max_refl',numRefl);
toc
%%
%Plots the impulse response
for ii = 1:numel(multi_set)
    figure('Renderer', 'painters', 'Position', [10 10 800 600]);
    subplot(2,1,1)
    plot(t*1e9,real(multi_set(ii).rsig));
%     title([num2str(ii),"constellation"])
    xlabel('t in ns')
    ylabel('Re[y(t)]')
    subplot(2,1,2)
    imp_resp = mag2db(abs(multi_set(ii).imp_resp));
    imp_resp(isinf(imp_resp)) = -200;
    stem(t*1e9,imp_resp,'BaseValue',-200);
    if ~isempty(multi_set.int_resp)
        hold on;
        int_resp = mag2db(abs(multi_set(ii).int_resp));
        int_resp(isinf(int_resp)) = -200;
        stem(t,int_resp,'BaseValue',-200,'color','red');
    end
    xlabel('t in ns')
    ylabel('Magnitude in dB')
end
%%
%Plot the Environment
plotEnvironment(multi_set);
%Plots the pattern
figure;
multi_set(1).antRx.antObj.pattern(60e9,-180:180,0,'Type','power','CoordinateSystem','rectangular','Normalize',false,'Weights',multi_set(1).antRx.weights)