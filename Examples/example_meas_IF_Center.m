%%
%  Interfernce scenario Community Center
numRefl = 2;
saveData = false;
runMeasCase = true;
% Position of TX and RX antenna
counter = input('Geben sie den Counter ein: '); 
txpos = [5,0];
rxpos = [5,-2];
IFtxpos = [4,0];
IFrxpos = [4,-2];

txdir = -90+45;
rxdir = 90-45;
IFtxdir = -90;
IFrxpdir = 90;
% Create a room with rectangular obstacles. [xmid,ymid,xlength,ylenght,rotation,epsilon]
obstacles = [];
room = [12,20];

%Antennaobjects of Tx and Rx;
antTx(1) = cAntenna('model',txpos,txdir,'enable_Beamforming',true);
antTx(2) = cAntenna('model',IFtxpos,IFtxdir,'enable_Beamforming',true);
antRx(1) = cAntenna('model',rxpos,rxdir,'enable_Beamforming',true);
antRx(2) = cAntenna('model',IFrxpos,IFrxpdir,'enable_Beamforming',true);
%% Creating all transmit signals
f_samp = 10e9;
t = 0:1/f_samp:100e-9; % Time Vector
t = t(1:end-1);
signal = cell(1,1);
signal1 = (sin(2*pi*100e6*t))';
signal2 = (sawtooth(2*pi*100e6*t))';
signal2(100:200) = 2;
signal{1} = signal1;
signal{2} = signal2;
%%
%Raytracing Simulation
tic
multi_set = multipath_propagation(signal,antTx,antRx,f_samp,'room',room,'obstacles',obstacles,'max_refl',numRefl,'eps_wall',3e9);
toc
disp(['SNR-MOD: ',num2str(pow2db(multi_set(1).SNR))])
%%
for ii = 1:size(multi_set,2)
%     figure
    figure('Renderer', 'painters', 'Position', [10 10 800 600])
    subplot(2,1,1)
    plot(t*1e9,real(multi_set(ii).rsig+multi_set(ii).interference));
%     title([num2str(ii),"constellation"])
    title('Received signal')
    xlabel('t in ns');
    ylabel('magnitude');
    subplot(2,1,2)
    imp_resp = mag2db(abs(multi_set(ii).imp_resp));
    imp_resp(isinf(imp_resp)) = -200;
    stem(t*1e9,imp_resp,'BaseValue',-200);
    IF_imp_resp = mag2db(abs(multi_set(ii).int_resp));
    IF_imp_resp(isinf(IF_imp_resp)) = -200;
    hold on;
    stem(t*1e9,IF_imp_resp,'BaseValue',-200,'color','red');
    title('Channel impulse response')
    xlabel('t in ns');
    ylabel('power in dB');
end

%%
plotEnvironment(multi_set);
if (runMeasCase)
    antTx(1) = cAntenna('measurements',txpos,txdir,'enable_Beamforming',true);
    antTx(2) = cAntenna('measurements',IFtxpos,IFtxdir,'enable_Beamforming',true);
    antRx(1) = cAntenna('measurements',rxpos,rxdir,'enable_Beamforming',true);
    antRx(2) = cAntenna('measurements',IFrxpos,IFrxpdir,'enable_Beamforming',true);
    tic
    multi_set_meas = multipath_propagation(signal,antTx,antRx,f_samp,'room',room,'obstacles',obstacles,'max_refl',numRefl,'eps_wall',3e9);
    toc
end
disp(['SNR-MEAS: ', num2str(pow2db(multi_set_meas(1).SNR))])
%%
%SAVE DATA
% if (saveData)
%     sectordataTX = generateTalonWeights(multi_set(1).antTx.weights,multi_set(1).antTx.type);
%     sectordataRX = generateTalonWeights(multi_set(1).antRx.weights,multi_set(1).antRx.type);
%     Save config just for information
%     distTxRx = sqrt((rxpos(1)-txpos(1))^2+(rxpos(2)-txpos(2))^2);
%     file = fopen('.\TalonWeights\los_config.txt','a+');
%     fprintf(file, 'TXPos[%.2f,%.2f] TXAngle[%d] RXPos[%.2f,%.2f] RXAngle[%d] DistTxRx %.2f', ...
%         txpos(1),txpos(2),txdir,rxpos(1),rxpos(2),rxdir,distTxRx);
%     fprintf(file, '\n');
%     fclose(file);
%     Save Talon Weights
%     filename = strcat(".\TalonWeights\Church\weights_model_Church_IF",num2str(counter),".txt");
%     file = fopen(filename,'w');
%     fprintf(file, sectordataTX.Talon);
%     fprintf(file, '\n');
%     fprintf(file, sectordataRX.Talon);
%     fclose(file);
%     matfile = strcat(".\ResultsMat\Church\multi_set_model_Church_IF",num2str(counter),".mat");
%     save(matfile,'multi_set');
%     if (runMeasCase)
%         Save MeasCase
%         Save TalonWeights
%         sectordataTX = generateTalonWeights(multi_set_meas(1).antTx.weights,multi_set_meas(1).antTx.type);
%         sectordataRX = generateTalonWeights(multi_set_meas(1).antRx.weights,multi_set_meas(1).antRx.type);
%         filename = strcat(".\TalonWeights\Church\weights_meas_Church_IF",num2str(counter),".txt");
%         file = fopen(filename,'w');
%         fprintf(file, sectordataTX.Talon);
%         fprintf(file, '\n');
%         fprintf(file, sectordataRX.Talon);
%         fclose(file);
%         Save ResutsMat
%         matfile = strcat(".\ResultsMat\Church\multi_set_meas_Church_IF",num2str(counter),".mat");
%         save(matfile,'multi_set_meas');
%     end
% end