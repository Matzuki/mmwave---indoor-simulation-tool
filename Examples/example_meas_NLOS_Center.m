%%
% NLOS Community center
% TODO: 
numRefl = 2;
saveData = false;
runMeasCase = true;
% Position of TX and RX antenna
% counter = input('Geben sie den Counter ein: '); 
txpos = [3.9,0];
rxpos = [3.9,-2.15];
txdir = -90-60;
rxdir = 90;
% Create a room with rectangular obstacles. [xmid,ymid,xlength,ylenght,rotation,epsilon]
obstacles = [3.9,-2.15+0.6+0.2,0.595,0.395,0,2.1];
room = [10,20];

%Antennaobjects of Tx and Rx;
antTx(1) = cAntenna('model',txpos,txdir,'enable_Beamforming',true);
antRx(1) = cAntenna('model',rxpos,rxdir,'enable_Beamforming',true);
%% Creating all transmit signals
f_samp = 10e9;
t = 0:1/f_samp:100e-9; % Time Vector
t = t(1:end-1);
signal = cell(1,1);
signal{1} = zeros(length(t),size(antTx,2));
signal{1} = (sin(2*pi*100e6*t))';
%%
%Raytracing Simulation
tic
multi_set = multipath_propagation(signal,antTx,antRx,f_samp,'room',room,'obstacles',obstacles,'max_refl',numRefl,'eps_wall',3e9);
toc
%%
for ii = 1:size(multi_set,2)
%     figure
    figure('Renderer', 'painters', 'Position', [10 10 800 600])
    subplot(2,1,1)
    plot(t*1e9,real(multi_set(ii).rsig));
%     title([num2str(ii),"constellation"])
    title('Received signal')
    xlabel('t in ns');
    ylabel('magnitude');
    subplot(2,1,2)
    imp_resp = mag2db(abs(multi_set(ii).imp_resp));
    imp_resp(isinf(imp_resp)) = -200;
    stem(t*1e9,imp_resp,'BaseValue',-200);
    title('Channel impulse response')
    xlabel('t in ns');
    ylabel('power in dB');
end
%%
plotEnvironment(multi_set);
disp(['SNR-MOD: ',num2str(pow2db(multi_set.SNR))])
if (runMeasCase)
    antTx(1) = cAntenna('measurements',txpos,txdir,'enable_Beamforming',true);
    antRx(1) = cAntenna('measurements',rxpos,rxdir,'enable_Beamforming',true);
    tic
    multi_set_meas = multipath_propagation(signal,antTx,antRx,f_samp,'room',room,'obstacles',obstacles,'max_refl',numRefl,'eps_wall',3e9);
    toc
end
disp(['SNR-MEAS: ', num2str(pow2db(multi_set_meas.SNR))])
%%
%SAVE DATA
% if (saveData)
%     sectordataTX = generateTalonWeights(multi_set.antTx.weights,multi_set.antTx.type);
%     sectordataRX = generateTalonWeights(multi_set.antRx.weights,multi_set.antRx.type);
%     %Save config just for information
%     distTxRx = sqrt((rxpos(1)-txpos(1))^2+(rxpos(2)-txpos(2))^2);
%     file = fopen('.\TalonWeights\los_config.txt','a+');
%     fprintf(file, 'TXPos[%.2f,%.2f] TXAngle[%d] RXPos[%.2f,%.2f] RXAngle[%d] DistTxRx %.2f', ...
%         txpos(1),txpos(2),txdir,rxpos(1),rxpos(2),rxdir,distTxRx);
%     fprintf(file, '\n');
%     fclose(file);
%     %Save Talon Weights
%     filename = strcat(".\TalonWeights\weights_model_LOS",num2str(counter),".txt");
%     file = fopen(filename,'w');
%     fprintf(file, sectordataTX.Talon);
%     fprintf(file, '\n');
%     fprintf(file, sectordataRX.Talon);
%     fclose(file);
%     matfile = strcat(".\ResultsMat\multi_set_model_LOS",num2str(counter),".mat");
%     save(matfile,'multi_set');
%     if (runMeasCase)
%         %Save MeasCase
%         %Save TalonWeights
%         sectordataTX = generateTalonWeights(multi_set_meas.antTx.weights,multi_set_meas.antTx.type);
%         sectordataRX = generateTalonWeights(multi_set_meas.antRx.weights,multi_set_meas.antRx.type);
%         filename = strcat(".\TalonWeights\weights_meas_LOS",num2str(counter),".txt");
%         file = fopen(filename,'w');
%         fprintf(file, sectordataTX.Talon);
%         fprintf(file, '\n');
%         fprintf(file, sectordataRX.Talon);
%         fclose(file);
%         %Save ResutsMat
%         matfile = strcat(".\ResultsMat\multi_set_meas_LOS",num2str(counter),".mat");
%         save(matfile,'multi_set_meas');
%     end
% end