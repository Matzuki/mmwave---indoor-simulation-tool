%%
% Simulates the interference case in the office room

numRefl = 2; %number of reflections
% Position of TX and RX antenna
txpos = [0,-1];
txdir = 90;

rxpos = [0,2];
rxdir = -90;

IFtxpos = [0.5,-1];
IFtxdir = 90;

IFrxpos = [0.5,2];
IFrxpdir = -90;

% Create a room with rectangular obstacles. [xmid,ymid,xlength,ylenght,rotation,epsilon]
% Office Room in the SEEMOO institute
obstacles = [0.18,3,2,0.01,deg2rad(0),3;... %Beamer screen
    -2.81,-2.595,0.02,0.86,deg2rad(0),1.57;... %Door1
    -0.19,-3.19,0.86,0.02,deg2rad(0),1.57;...  %Door2
    1.52,-2.98,1.2,0.44,deg2rad(0),1.57;...    %closet1
    -2.641,3.1,0.36,0.2,0,2.81;... %pillar1A with -2.64 error
    -2.3725,3.165,0.175,0.07,0,2.1;... %cable duct
    2.32,3.13,0.44,0.14,0,6.14;... %pillar2A
    2.82,0,0.05,6.3,0,5.92;... %window
    2.78,3.19,0.2,0.02,0,3e9;... %windowframe1
    2.78,2.5425,0.2,0.125,0,3e9;... %windowframe2
    2.78,1.8525,0.2,0.125,0,3e9;... %windowframe3
    2.78,0.49,0.2,0.08,0,3e9;... %windowframe4
    2.78,-0.2125,0.2,0.125,0,3e9;... %windowframe5
    2.78,-0.8425,0.2,0.125,0,3e9;... %windowframe6
    2.78,-2.218,0.2,0.105,0,3e9;... %windowframe7
    2.78,-2.913,0.2,0.125,0,3e9;... %windowframe8
    -2.82,-0.01,0.1,2.09,0,3e9;... %Metallic board
    0,3.2,5.64,0.01,0,6.81;... %Concrete Wall
    1.77,2.5,1.24,0.1,deg2rad(130),2.1]; %TV

room = [5.64+0.02,6.4+0.02];

%Antennaobjects of Tx and Rx;
antTx(1) = cAntenna('model',txpos,txdir,'enable_Beamforming',true);
antTx(2) = cAntenna('model',IFtxpos,IFtxdir,'enable_Beamforming',true);
antRx(1) = cAntenna('model',rxpos,rxdir,'enable_Beamforming',true);
antRx(2) = cAntenna('model',IFrxpos,IFrxpdir,'enable_Beamforming',true);
%% Creating all transmit signals
f_samp = 10e9;
t = 0:1/f_samp:100e-9; % Time Vector
t = t(1:end-1);
signal = cell(1,1);
signal1 = (sin(2*pi*100e6*t)+2)';
signal2 = zeros(size(t))';
signal2(100:200) = 1;
signal{1} = signal1;
signal{2} = signal2;
%%
%Raytracing Simulation
tic
multi_set = multipath_propagation(signal,antTx,antRx,f_samp,'room',room,'obstacles',obstacles,'max_refl',numRefl,'eps_wall',2.81);
toc
%%
for ii = 1:size(multi_set,2)
%     figure
    figure('Renderer', 'painters', 'Position', [10 10 800 600])
    subplot(2,1,1)
    plot(t*1e9,real(multi_set(ii).rsig));
%     title([num2str(ii),"constellation"])
    title('Received signal')
    xlabel('t in ns');
    ylabel('magnitude');
    subplot(2,1,2)
    imp_resp = mag2db(abs(multi_set(ii).imp_resp));
    imp_resp(isinf(imp_resp)) = -200;
    stem(t*1e9,imp_resp,'BaseValue',-200);
    title('Channel impulse response')
    xlabel('t in ns');
    ylabel('power in dB');
end
%%
plotEnvironment(multi_set);
%% For dataSave purpose
% sectordata = generateTalonWeights(multi_set.antTx.weights,multi_set.antTx.type);
% distTxRx = sqrt((rxpos(1)-txpos(1))^2+(rxpos(2)-txpos(2))^2);
% file = fopen('.\TalonWeights\IF_config.txt','a+');
% fprintf(file, 'TXPos[%.2f,%.2f] TXAngle[%d] RXPos[%.2f,%.2f] RXAngle[%d] DistTxRx %.2f', ...
%     txpos(1),txpos(2),txdir,rxpos(1),rxpos(2),rxdir,distTxRx);
% fprintf(file, '\n');
% fclose(file);
% filename = strcat(".\TalonWeights\weights_model_IF",num2str(counter),".txt");
% file = fopen(filename,'w');
% fprintf(file, sectordata.Talon);
% fprintf(file, '\n');
% fclose(file);
% matfile = strcat(".\ResultsMat\multi_set_model_IF",num2str(counter),".mat");
% save(matfile,'multi_set');
% 
% antTx(1) = cAntenna('measurements',txpos,txdir,'enable_Beamforming',true);
% tic
% multi_set = multipath_propagation(signal,antTx,antRx,f_samp,'room',room,'obstacles',obstacles,'max_refl',numRefl,'eps_wall',2.81);
% toc
% sectordata = generateTalonWeights(multi_set.antTx.weights,multi_set.antTx.type);
% filename = strcat(".\TalonWeights\weights_meas_IF",num2str(counter),".txt");
% file = fopen(filename,'w');
% fprintf(file, sectordata.Talon);
% fprintf(file, '\n');
% fclose(file);
% matfile = strcat(".\ResultsMat\multi_set_meas_IF",num2str(counter),".mat");
% save(matfile,'multi_set');