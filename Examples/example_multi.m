%%
% Simulate a indoor multipath propagation with a model of a front antenna
% of the TP-Link Talon 60 GHz Router
% TODO: Compare Sectors with choosed path
%       Change signal to an AM Signal

% close all
numRefl = 2;
t = 0:1/10e9:100e-9; % Time Vector
impulse_response = getDiracSignal(t);
signal1 = zeros(size(t'));
signal2 = zeros(size(t'));
signal3 = zeros(size(t'));
signal1(100:200) = 1;
signal2(400:450) = 1;
signal3(600:700) = 1;
signal = cell(1,3);
signal{1} = signal1;
signal{2} = signal2;
signal{3} = signal3;
%%
f_samp = 10e9;
% Create a room with rectangular obstacles. [xmid,ymid,xlength,ylenght,rotation,epsilon]
% obstacles = [1,0,1,1,deg2rad(45),3;...
%     0,-1,2.5,0.5,deg2rad(0),3];
obstacles = [];
room = [10,10];
%Azimuth direction of zero direction of the antenna


%Antennaobjects of Tx and Rx;

antTx(1) = cAntenna('model',[0,-4],90,'enable_Beamforming',true);
antRx(1) = cAntenna('model',[2,4],-90,'enable_Beamforming',true);
antTx(2) = cAntenna('model',[1,-4],90,'enable_Beamforming',true);
antRx(2) = cAntenna('model',[0,4],-90,'enable_Beamforming',true);
antTx(3) = cAntenna('model',[2,-4],90,'enable_Beamforming',true);
antRx(3) = cAntenna('model',[-1,2],-90,'enable_Beamforming',true);

%Raytracing Simulation
tic
multi_set = multipath_propagation(signal,antTx,antRx,f_samp,'room',room,'obstacles',obstacles,'max_refl',numRefl);
toc
%%
for ii = 1:numel(multi_set)
    figure
    subplot(2,1,1)
%     plot(t,abs(multi_set(ii).rsig));
%     plot(t,abs(multi_set(ii).rsig+multi_set(ii).interference));
    plot(t,abs(multi_set(ii).rsig));
    title([num2str(ii),"constellation"])
    
    subplot(2,1,2)
    imp_resp = mag2db(abs(multi_set(ii).imp_resp));
    imp_resp(isinf(imp_resp)) = -200;
    stem(t,imp_resp,'BaseValue',-200);
    hold on;
    int_resp = mag2db(abs(multi_set(ii).int_resp));
    int_resp(isinf(int_resp)) = -200;
    stem(t,int_resp,'BaseValue',-200,'color','red');
end
%%
plotEnvironment(multi_set);
%Draw Pattern of first antenna
figure;
multi_set(1).antRx.antObj.pattern(60e9,-180:180,0,'Type','efield','CoordinateSystem','rectangular','Normalize',false,'Weights',multi_set(1).antRx.weights)