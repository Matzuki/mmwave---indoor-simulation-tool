%%
% Simulates the distance reference measurement inside the community center
SNR_mod= zeros(1,51);
SNR_meas = zeros(1,51);
index = 1;
for counter=10:1:60
    numRefl = 1;
    saveData = false;
    runMeasCase = false;
    % Position of TX and RX antenna
    % counter = input('Geben sie den Counter ein: '); 
    txpos = [0,-5+counter/10];
    rxpos = [0,-5];
    txdir = -90;
    rxdir = 90;
    % Create a room with rectangular obstacles. [xmid,ymid,xlength,ylenght,rotation,epsilon]
    obstacles = [];
    room = [20,15];

    %Antennaobjects of Tx and Rx;
    antTx(1) = cAntenna('model',txpos,txdir,'enable_Beamforming',true);
    % antRx(1) = cAntenna('isotropic',rxpos,rxdir,'enable_Beamforming',true);
    antRx(1) = cAntenna('model',rxpos,rxdir,'enable_Beamforming',true);
    %% Creating all transmit signals
    f_samp = 10e9;
    t = 0:1/f_samp:100e-9; % Time Vector
    t = t(1:end-1);
    signal = cell(1,1);
    signal{1} = zeros(length(t),size(antTx,2));
    signal{1} = (sin(2*pi*100e6*t))';
    %%
    %Raytracing Simulation
    tic
    multi_set = multipath_propagation(signal,antTx,antRx,f_samp,'room',room,'obstacles',obstacles,'max_refl',numRefl,'eps_wall',2.81);
    toc
    %%
    disp(['SNR_MOD: ',num2str(pow2db(multi_set.SNR))])
    SNR_mod(index) = pow2db(multi_set.SNR);
    index = index+1;
end
%%
plot(1:0.1:6,(SNR_mod))
hold on;
if (runMeasCase)
    plot(1:0.1:6,SNR_meas)
    legend('Mod','Meas');
end