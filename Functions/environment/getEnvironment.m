%GETENVIRONMENT Create the environment matrix including room dimensions and
%all obstacles(if exist)
%
% 	Project:        mmWave - Indoor Simulation Tool
% 	Author:         Mathias Marscholl
% 	Affiliation:    SEEMOO, TU Darmstadt
% 	Date: 			08.10.2019
%   Input: 
%   room: row vector of the room of size [n,m]
%   epswall: permitivity of the walls.
%   obstacles: matrix with all defined obstacles
%
%   Output: Environment matrix
function [environment] = getEnvironment(room,epswall,obstacles)
    class = {'numeric'};
    attributeRoom = {'size',[1,2]}; 
    attributeObst = {'size',[NaN,6]};

    if ~isempty(obstacles)
        validateattributes(room,class,attributeRoom);
        validateattributes(obstacles,class,attributeObst);
        room = setRoom(room(1),room(2));
        room = [room,epswall];
        environment = [room; obstacles];
    else
        validateattributes(room,class,attributeRoom)
        room = setRoom(room(1),room(2));
        environment = [room,epswall];
    end
end