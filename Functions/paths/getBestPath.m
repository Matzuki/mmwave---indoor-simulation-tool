% GETBESTPATH calculate for the best path the beamforming
% direction and the weights. 
%  
% 	Project:        mmWave - Indoor Simulation Tool
% 	Author:         Mathias Marscholl
% 	Affiliation:      SEEMOO, TU Darmstadt
% 	Date:            19.10.2019
%
%   Input: 
%   result: The result structure including the antennas and all path
%   informations
%   radiator: Radiator of the TX antenna
%   txangle: All tx angles for the different paths
%   collector: collector of the RX antenna
%   rxangle: All rx angles for the different paths   
%
%   Output:
%   wTx: Tx-Beamforming weights for the best paths
%   wRx: Rx-Beamforming weights for the best paths
%   bfaTx: Tx-steering direction towards the best path
%   bfaRx: Rx-steering direction towards the best path
%   bestPathID: ID of the best path
function [wTx,wRx,bfaTx,bfaRx,bestPathID] = getBestPath(results,radiator,txangle,collector,rxangle)
    if(results.antTx.enableBeamforming && results.antRx.enableBeamforming)
        wTx = results.antTx.getBeamformingWeights(txangle');
        wRx = results.antRx.getBeamformingWeights(rxangle');
        
        %nFactor = max(results.antTx.antObj.pattern(60e9,-180:180,0,'Type','efield','CoordinateSystem','rectangular','Normalize',false));
        nFactor = 1;
        gainTx  = ones(size(txangle,1),1);
        for ii = 1:size(wTx,2)
            gainTx(ii,1) = abs(radiator(1,txangle(ii),conj(wTx(:,ii))))/nFactor;
        end
        %nFactor = max(results.antRx.antObj.pattern(60e9,-180:180,0,'Type','efield','CoordinateSystem','rectangular','Normalize',false));
        nFactor = 1;
        gainRx  = ones(size(rxangle,1),1);
        for ii = 1:size(wRx,2)
            temp = collector(1,rxangle(ii),conj(wRx(:,ii)))/nFactor;
            gainRx(ii,1) = abs(sum(temp,2));
        end
        [~,index_best] = max(abs(results.p_loss{1}).*results.refl_coef{1}.*gainTx.*gainRx);
        wTx = wTx(:,index_best);
        wRx = wRx(:,index_best);
        bfaTx = txangle(index_best);
        bfaRx = rxangle(index_best);
        bestPathID = index_best;
    % Only TX Beamforming
    elseif (results.antTx.enableBeamforming && ~results.antRx.enableBeamforming)
        wTx = results.antTx.getBeamformingWeights(txangle');
%         nFactor = max(results.antTx.antObj.pattern(60e9,-180:180,0,'Type','efield','CoordinateSystem','rectangular','Normalize',false));
        nFactor = 1;
        gainTx  = ones(size(txangle,1),1);
        for ii = 1:size(wTx,2)
            gainTx(ii,1) = abs(radiator(1,txangle(ii),conj(wTx(:,ii))))/nFactor;
        end
%         nFactor = max(results.antRx.antObj.pattern(60e9,-180:180,0,'Type','efield','CoordinateSystem','rectangular','Normalize',false));
        nFactor = 1;
        gainRx  = ones(size(rxangle,1),1);
        for ii = 1:size(rxangle,1)
            temp = collector(1,rxangle(ii))/nFactor;
            gainRx(ii,1) = abs(sum(temp,2));
        end
        [~,index_best] = max(abs(results.p_loss{1}).*results.refl_coef{1}.*gainTx.*gainRx);
        wTx = wTx(:,index_best);
        wRx = [];
        bfaTx = txangle(index_best);
        bfaRx = rxangle(index_best);
        bestPathID = index_best;
    % Only RX Beamforming
    elseif (~results.antTx.enableBeamforming && results.antRx.enableBeamforming)
        wRx = results.antRx.getBeamformingWeights(rxangle');
%         nFactor = max(results.antTx.antObj.pattern(60e9,-180:180,0,'Type','efield','CoordinateSystem','rectangular','Normalize',false));
        nFactor = 1;
        gainTx  = ones(size(txangle,1),1);
        for ii = 1:size(txangle,1)
            gainTx(ii,1) = abs(radiator(1,txangle(ii)))/nFactor;
        end
%         nFactor = max(results.antRx.antObj.pattern(60e9,-180:180,0,'Type','efield','CoordinateSystem','rectangular','Normalize',false));
        nFactor = 1;
        gainRx  = ones(size(rxangle,1),1);
        for ii = 1:size(wRx,2)
            temp = collector(1,rxangle(ii),conj(wRx(:,ii)))/nFactor;
            gainRx(ii,1) = abs(sum(temp,2));
        end
        [~,index_best] = max(abs(results.p_loss{1}).*results.refl_coef{1}.*gainTx.*gainRx);
        wTx = [];
        wRx = wRx(:,index_best);
        bfaTx = txangle(index_best);
        bfaRx = rxangle(index_best);
        bestPathID = index_best;
    else
        nFactor = 1;
        gainTx  = ones(size(txangle,1),1);
        for ii = 1:size(txangle,1)
            gainTx(ii,1) = abs(radiator(1,txangle(ii)))/nFactor;
        end
        gainRx  = ones(size(rxangle,1),1);
        for ii = 1:size(rxangle,1)
            temp = collector(1,rxangle(ii))/nFactor;
            gainRx(ii,1) = abs(sum(temp,2));
        end
        [~,index_best] = max(abs(results.p_loss{1}).*results.refl_coef{1}.*gainTx.*gainRx);
        wTx = [];
        wRx = [];
        bfaTx = [];
        bfaRx = [];
        bestPathID = index_best;
    end
end