%LOSSES_DELAY Get the receiving signal after propagation with the corresponding delay
%and pathloss. The choosen Pathloss Model is the CI (Close-In) model from 
%[1]. The model is only valid for distances d>=1m;
%   
% 	Project: 		mmTrace - Real Antenna
% 	Author: 		Mathias Marscholl
% 	Affiliation:	SEEMOO, TU Darmstadt
% 	Date: 			08.10.2019
%
%   Input:
%   sig: the transmitted signal
%   delay: the calculated delay according to the ray tracing algorithm
%   distance: the distance from transmitter to receiver of all different paths.
%
%   Output:
%   delay_samples: Number of sample each paths is delayed
%   PathLoss: Pathloss for each path
%
%   [1]: Directional Radio Propagation Path Loss Models
%   for Millimeter-Wave Wireless Networks in the
%   28-, 60-, and 73-GHz Bands
function [delay_samples,PathLoss] = losses_delay(delay,distance,f_samp)
n = 2.2;
fc = 60e9;
c = physconst('lightspeed');
lambda = c/fc;
phase = exp(-1i*2*pi/lambda*distance);
% Standard Physical Model
%     PathLoss = fspl(distance,lambda);
%     PathLoss = 10.^(-PathLoss./20);

% CI Modell
PlCI = ones(size(distance));
for ii = 1:length(distance)
    if distance(ii)<3
%         PlCI(ii) = 20*log10(fc*4*pi/c)+10*n*log10(distance(ii))-3.3333*distance(ii)+8.3333;
        PlCI(ii) = 20*log10(fc*4*pi/c)+10*n*log10(distance(ii))-3.75*distance(ii)+11.25;
    else
        PlCI(ii) = 20*log10(fc*4*pi/c)+10*n*log10(distance(ii));
    end
end
    PathLoss = 10.^(-PlCI./20);
    PathLoss = PathLoss.*phase;
    delay_samples = round(delay*f_samp);
end

