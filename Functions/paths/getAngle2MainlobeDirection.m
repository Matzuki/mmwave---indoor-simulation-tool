% GETANGLE2MAINLOBEDIRECTION Gets the relative Angle of Arrival or Angle 
% of depature with respect to the antenna zero direction
%  
% 	Project:    mmWave - Indoor Simulation Tool
% 	Author:     Mathias Marscholl
% 	Affiliation:   SEEMOO, TU Darmstadt
% 	Date:         08.10.2019
%   
%   Input:  
%   absoluteAngle: the angle (in degree) in relation to the room reference
%   system
%
%   antennaZeroDirection: the angle (in degree) of the Zero direction of the 
%   antenna in relation to the room reference system
%
%   Output:
%   relAngle: The relative angle from the antenna zero Direction
%   in degree
%   
%   Example: The antenna zero lies at 30� in the room and the absolute
%   AoA is 45�(room reference). The angle with respect to the antenna zero
%   is 15�
function [relAngle] = getAngle2MainlobeDirection(absoluteAngle,antennaZeroDirection)
    relAngle = absoluteAngle-antennaZeroDirection;
    relAngle = wrapTo180(relAngle);
end