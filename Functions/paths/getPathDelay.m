%   Calculate the timedelay of the different paths according to the speed 
%   of light
%   
% 	Project:        mmTrace - Real Antenna
% 	Author:         Mathias Marscholl
% 	Affiliation:    SEEMOO, TU Darmstadt
% 	Date: 			22.03.2019
%
%   Input: 
%   p1: Position of the Transmitter
%   p2: Position of the Receiver
%   paths: paths from the trancePaths function.
%   
%   Output: Corresponding timedelay due to propagation with lightspeed.
function [ delay ] = getPathDelay(p1,p2,paths)
    lengths = sqrt((p1(1)-paths(:,1)).^2 + (p1(2)-paths(:,2)).^2) + sqrt((p2(1)-paths(:,1)).^2 + (p2(1)-paths(:,2)).^2);
    delay = lengths/physconst('lightspeed');
end

