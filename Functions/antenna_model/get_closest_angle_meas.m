%GET_CLOSEST_ANGLE_MEAS  Find the closest common angle between the wanted
%   steering angle and the actual defined azimuth angles. Function only for
%   the measured antenna object
%
% 	Project:        mmWave - Indoor Simulation Tool
% 	Author:         Mathias Marscholl
% 	Affiliation:    SEEMOO, TU Darmstadt
% 	Date: 			08.10.2019

function [angle] = get_closest_angle_meas(steerangle,azang)
    if(isvector(steerangle)&&iscolumn(steerangle))
        steerangle=steerangle.';
    elseif(~isvector(steerangle)&&isequal(size(steerangle,1),2))
        steerangle=steerangle(1,:);
    elseif(~isvector(steerangle)&&isequal(size(steerangle,2),2))
        steerangle=steerangle(:,1)';
    end
    [~,index] = min(abs(azang - steerangle));
    angle = azang(index);
end

