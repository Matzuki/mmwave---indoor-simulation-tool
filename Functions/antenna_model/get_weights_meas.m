%GET_WEIGHTS_MEAS gets the antenna steering weights for the given angle 
%for the measurements object 
%
% 	Project:        mmWave - Indoor Simulation Tool
% 	Author:         Mathias Marscholl
% 	Affiliation:    SEEMOO, TU Darmstadt
% 	Date: 			08.10.2019
function [w] = get_weights_meas(steer_angle,azang)
load('arrayTable.mat');
antennapart = 'all';

if(strcmp(antennapart,'front'))
    active_Element = zeros(32,1);
    active_Element(2+1) = 1;
    active_Element(7+1) = 1;
    active_Element(4+1) = 1;
    active_Element(13+1) = 1;
    active_Element(15+1) = 1;
    active_Element(9+1) = 1;

    active_Element(17+1) = 1;
    active_Element(23+1) = 1;
    active_Element(21+1) = 1;
    active_Element(31+1) = 1;
    active_Element(27+1) = 1;
    active_Element(26+1) = 1;
    max_elements = 16;
elseif(strcmp(antennapart,'back'))
    active_Element = zeros(32,1);
    active_Element(3+1) = 1;
    active_Element(10+1) = 1;
    active_Element(12+1) = 1;
    active_Element(18+1) = 1;
    active_Element(19+1) = 1;
    active_Element(20+1) = 1;
    max_elements = 6;
elseif(strcmp(antennapart,'dipole'))
    active_Element = zeros(32,1);
    active_Element(0+1) = 1;
    active_Element(1+1) = 1;
    active_Element(5+1) = 1;
    active_Element(6+1) = 1;
    active_Element(8+1) = 1;
    active_Element(11+1) = 1;

    active_Element(14+1) = 1;
    active_Element(16+1) = 1;
    active_Element(22+1) = 1;
    active_Element(24+1) = 1;
    active_Element(25+1) = 1;
    
    active_Element(28+1) = 1;
    active_Element(29+1) = 1;
    active_Element(30+1) = 1;
    max_elements = 14;
elseif(strcmp(antennapart,'all'))
    active_Element = ones(32,1);
    max_elements = 14;
end

closestAngle = get_closest_angle_meas(steer_angle,azang);
for ii = 1:length(closestAngle)
    gain = arrayTable.cgain(arrayTable.pan==closestAngle(ii));
    gain(isnan(gain))=0;

    wtemp = round(abs(log10(gain))./max(abs(log10(gain)))*6);
    wtemp = active_Element.*wtemp;
    gain = gain.*active_Element;
    treshold = min(maxk(abs(gain),max_elements));
    wtemp(abs(gain)<treshold) = 0;

    gain = conj(gain);
    phase = round((angle(gain)/ pi *2));
    phase = mod(phase,4);
    wtemp = ones(32,1).*(wtemp>0);
    wtemp = wtemp.*exp(1i*pi/2*phase);
    w(:,ii) = wtemp;
end
end

