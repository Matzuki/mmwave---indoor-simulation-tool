%GETANTENNAELEMENTS returns the requestet antenna element 
%
% 	Project:        mmWave - Indoor Simulation Tool
% 	Author:         Mathias Marscholl
% 	Affiliation:    SEEMOO, TU Darmstadt
% 	Date: 			08.10.2019
%
%   Input:
%   antennaType: String with the wanted antenna type (Talon Model, Talon
%   measurements, isotropic, cosine)
%
%   Output:
%   antElement: Return the wanted antenna element
function [ antElement] = getAntennaElements( antennaType ) 
if ischar(antennaType)
    antennaType = convertCharsToStrings(antennaType);
end
if ~isstring(antennaType)
    error("Input must be a string or char")
end
if strcmp(antennaType,"model")
%     load('antenna_array.mat');
%     antElement = antenna_array;
    %load('modified_model.mat');
    load('patternSynt.mat');
    antElement = antenna_array;
    %Taper Scalieren f�r gute SNR Werte
%     antElement.Taper = antenna_array.Taper*0.127; %0.127 war gut mit 5e-9 noise
    antElement.Taper = antenna_array.Taper*0.03*1.75;%*1.7;
%     antElement = modified_model;
elseif strcmp(antennaType,"measurements")
    load("measurement_antenna_array.mat");
    antElement = oarray;
    antElement.release();
    %Taper Scalieren f�r gute SNR Werte
    antElement.Taper = antElement.Taper*20e-6*1.7;
elseif strcmp(antennaType,"isotropic")
%     antElement = phased.IsotropicAntennaElement();
    azimuth = -180:180;
    ele = -1:1;
    magPattern = ones(length(ele),length(azimuth))*3;
    magPattern = mag2db(magPattern);
    phasePattern = zeros(size(magPattern));
    
    antElement = phased.CustomAntennaElement('FrequencyVector',[55e9,60e9,65e9],'FrequencyResponse',[0 0 0],'AzimuthAngles',azimuth,'ElevationAngles',ele,'MagnitudePattern',magPattern,'PhasePattern',phasePattern);
else
    error("No matching input String. Use ""model"",""measurements"" or ""isotropic""");
end
end
