% RELATIVE_ANGLES Calculate the relative direction to the mainlobe 
% direction from the absolute direction. The output is a 
% length(paths) x 2 matrix with [az,el] in each row.

% 	Project:        mmWave - Indoor Simulation Tool
% 	Author:         Mathias Marscholl
% 	Affiliation:    SEEMOO, TU Darmstadt
% 	Date: 			08.10.2019
function relative_angles = relAngle(paths,direction,type)
    if(strcmp(type,'TX'))
        relative_angles = rad2deg(getPathTXAngle(paths));
    elseif(strcmp(type,'RX'))
        relative_angles = rad2deg(getPathRXAngle(paths));
    else
        error('Type must be ''TX'' or ''RX''!');
    end
    relative_angles = getAngle2MainlobeDirection(relative_angles,direction);
    relative_angles = [relative_angles,zeros(length(relative_angles),1)];
end