%   FORMATSTRING gets the string with the antenna weights to use it with
%   the talons
%
% 	Project:        mmWave - Indoor Simulation Tool
% 	Author:         Mathias Marscholl
% 	Affiliation:    SEEMOO, TU Darmstadt
% 	Date: 			08.10.2019
%
%   Input:
%   phase: Phase values for the antenna network
%   etype: Amplifier values for the antenna amplifier
%   dtype: Amplifier values for the distribution amplifier
%
%   Output: String to use the given antenna weights for the talons

function [formated_str] = formatString(phase,etype,dtype)
    formated_str = "{'psh': [";
    for ii=1:length(phase)
        if ~isequal(ii,length(phase))
            formated_str = strcat(formated_str,num2str(phase(ii)),", ");
        else
             formated_str = strcat(formated_str,num2str(phase(ii)),",]");
        end
    end
    formated_str = strcat(formated_str,", 'etype': [");
    for ii=1:length(etype)
        if ~isequal(ii,length(etype))
            formated_str = strcat(formated_str,num2str(etype(ii)),", ");
        else
             formated_str = strcat(formated_str,num2str(etype(ii)),",]");
        end
    end
    formated_str = strcat(formated_str,", 'dtype': [");
    for ii=1:length(dtype)
        if ~isequal(ii,length(dtype))
            formated_str = strcat(formated_str,num2str(dtype(ii)),", ");
        else
             formated_str = strcat(formated_str,num2str(dtype(ii)),",]");
        end
    end
    formated_str = strcat(formated_str,", 'x16': 200}");
end

