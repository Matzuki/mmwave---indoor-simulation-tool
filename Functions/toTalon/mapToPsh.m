%   MAPTOPSH maps the phase values to the 2bit phaseshifter of the talons
%   (0�,90�,180�,270�)
%
% 	Project:        mmWave - Indoor Simulation Tool
% 	Author:         Mathias Marscholl
% 	Affiliation:    SEEMOO, TU Darmstadt
% 	Date: 			08.10.2019
%
%   Input:
%   pshift: phaseshift values
%
%   Output:
%   bitvec: the bitvector for the talon phaseshifts
function [bitvec] = mapToPsh(pshifts)
%Maps the Phaseshifter values to the talon values 0,1,2 and 3.
    bitvec = zeros(size(pshifts));
    pshifts = round(rad2deg(angle(pshifts)));
    for i=1:length(pshifts)
        if(pshifts(i)==0)
            bitvec(i)=0;
        elseif(pshifts(i)==90)
            bitvec(i)=1;
        elseif(pshifts(i)==180)
            bitvec(i)=2;
        elseif(pshifts(i)==-90)
            bitvec(i)=3;
        end
    end
end

