%   GENERATETALONWEIGHTS gets the json formatted string for the talon
%   antenna weighting network
%
% 	Project:        mmWave - Indoor Simulation Tool
% 	Author:         Mathias Marscholl
% 	Affiliation:    SEEMOO, TU Darmstadt
% 	Date: 			08.10.2019
%
%   Input:
%   weights: Calculated antenna weights
%   anttype: weights for the choosed antenna type (model or measurements)
%
%   Output: json formated string for the talons

function j = generateTalonWeights(weights,anttype)
%Calculates the json sector representation for the Python plotter(first
%row) and for the talon routers directly. 
    etype = zeros(1,32);
    dtype = ones(1,8)*7;
    if isequal(anttype,'model')
        etype(2+1) = 7;
        etype(4+1) = 7;
        etype(7+1) = 7;
        etype(9+1) = 7;
        etype(13+1) = 7;
        etype(15+1) = 7;
        etype(17+1) = 7;
        etype(21+1) = 7;
        etype(23+1) = 7;
        etype(26+1) = 7;
        etype(27+1) = 7;
        etype(31+1) = 7;
        weights = mapToPsh(weights);
        psh = zeros(1,32);
        psh(2+1) = weights(1);
        psh(4+1) = weights(2);
        psh(7+1) = weights(3);
        psh(9+1) = weights(4);
        psh(13+1) = weights(5);
        psh(15+1) = weights(6);
        psh(17+1) = weights(7);
        psh(21+1) = weights(8);
        psh(23+1) = weights(9);
        psh(26+1) = weights(10);
        psh(27+1) = weights(11);
        psh(31+1) = weights(12);
        
        j = struct('psh',psh,'etype',{etype},'dtype',{dtype},'x16',200);
        j_py = jsonencode(j);
        j_talon = formatString(psh,etype,dtype);
        j = struct('Python',j_py,'Talon',j_talon);
        %j = vertcat(j_py,j_talon);
    elseif isequal(anttype,'measurements')
        etype(abs(weights)~=0) = 7;
        psh = mapToPsh(weights);
        j = struct('psh',psh,'etype',{etype},'dtype',{dtype},'x16',200);
        j_py = jsonencode(j);
        j_talon = formatString(psh,etype,dtype);
        j = struct('Python',j_py,'Talon',j_talon);
    end
end

