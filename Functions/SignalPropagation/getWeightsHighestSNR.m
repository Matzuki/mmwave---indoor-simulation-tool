%   GETWEIGHTSHIGHESTSNR gets the best path according
%   the GETBESTPATH function
%
% 	Project:        mmWave - Indoor Simulation Tool
% 	Author:         Mathias Marscholl
% 	Affiliation:    SEEMOO, TU Darmstadt
% 	Date: 			08.10.2019
%
%   Input:
%   results: resulting struct containing all important values
%   
%
%   Output:
%   results: modified resulting struct

function [results] = getWeightsHighestSNR(results)
        radiator = phased.Radiator('Sensor',results.antTx.antObj,'OperatingFrequency',60e9,'WeightsInputPort',results.antTx.enableBeamforming);
        collector = phased.Collector('Sensor',results.antRx.antObj,'OperatingFrequency',60e9,'WeightsInputPort',results.antRx.enableBeamforming);

        [wTx,wRx,bfaTx,bfaRx,bestPathID] = getBestPath(results,radiator,results.txAngle{1},collector,results.rxAngle{1});
        results.antTx.weights = wTx;
        results.antRx.weights = wRx;
        results.antTx.steering_direction = bfaTx;
        results.antRx.steering_direction = bfaRx;
end

