%   GETRSIG calculate the receiving signal, the channel impulse response,
%   interference values and the SNR
%
% 	Project:        mmWave - Indoor Simulation Tool
% 	Author:         Mathias Marscholl
% 	Affiliation:    SEEMOO, TU Darmstadt
% 	Date: 			08.10.2019
%
%   Input:
%   results: resulting struct containing all important values
%   
%
%   Output:
%   rsig: received signa
%   imp_resp: channel impulse response
%   sumIF:  interference Signal
%   sumIF_resp: interference components of the channel impulse response
%   SNR: signal to noise ratio

function [rsig,imp_resp,sumIF,sumIF_resp,SNR] = getRSig(results)
    nPower = 1e-11; %5e-9
    rng(1);
    dirac = getDiracSignal(results.sig{1});
    radiator = phased.Radiator('Sensor',results.antTx.antObj,'OperatingFrequency',60e9,'WeightsInputPort',results.antTx.enableBeamforming);
    collector = phased.Collector('Sensor',results.antRx.antObj,'OperatingFrequency',60e9,'WeightsInputPort',results.antRx.enableBeamforming);
    %Part1 TxRx Communication
    % TX Beamforming 
    if results.antTx.enableBeamforming
        radiator.release();
        rsig = radiator(results.sig{1},results.txAngle{1}',conj(results.antTx.weights));
        dirac = radiator(dirac,results.txAngle{1}',conj(results.antTx.weights));
    else
        radiator.release();
        rsig = radiator(results.sig{1},results.txAngle{1}');
        dirac = radiator(dirac,results.txAngle{1}');
    end
    % Signal propagation including pathloss, delay, and reflection losses    
    
    rsig = rsig*diag(results.p_loss{1});
    rsig = delayseq(rsig,results.delay_samp{1});

    dirac = dirac*diag(results.p_loss{1});
    dirac = delayseq(dirac,results.delay_samp{1});

    phase_refl = phaseRefl(results.refAngle{1});

    rsig = rsig*diag(results.refl_coef{1}.*phase_refl);
    dirac = dirac*diag(results.refl_coef{1}.*phase_refl);
    % Adding some Noise
%     npower = 60e-6;
%     noisesig = sqrt(npower/2)*(randn(size(rsig)) + 1i*randn(size(rsig)));
%     rsig = rsig + noisesig;
%     dirac = dirac + noisesig;
    
    % RX Beamforming 
    if results.antRx.enableBeamforming
        collector.release();
        rsig = collector(rsig,results.rxAngle{1}',conj(results.antRx.weights));
        rsig = sum(rsig,2);
        SPower = sum(abs(rsig).^2)/length(rsig);
        SNR = SPower/nPower;
        
        collector.release();
        dirac = collector(dirac,results.rxAngle{1}',conj(results.antRx.weights));
        dirac = sum(dirac,2);
        imp_resp = sum(dirac,2);
    else
        collector.release();
        rsig = collector(rsig,results.rxAngle{1}');        
        rsig = sum(rsig,2);
        SPower = sum(abs(rsig).^2)/length(rsig);
        SNR = SPower/nPower;
        
        dirac = collector(dirac,results.rxAngle{1}');
        imp_resp = sum(dirac,2);
    end
    
    %Interference Part
    if ~isempty(results.antIF)
        sumIF = zeros(size(results.sig,1));
        sumIF_resp = zeros(size(results.sig,1));
        for i_IF = 1:numel(results.antIF)
            dirac = getDiracSignal(results.sig{1});
            radiator = phased.Radiator('Sensor',results.antIF(i_IF).antObj,'OperatingFrequency',60e9,'WeightsInputPort',results.antIF(i_IF).enableBeamforming);
            collector = phased.Collector('Sensor',results.antRx.antObj,'OperatingFrequency',60e9,'WeightsInputPort',results.antRx.enableBeamforming);
            %Part1 TxRx Communication
            % TX Beamforming 
            if results.antIF(i_IF).enableBeamforming
                radiator.release();
                IF = radiator(results.sig{1},results.txAngle{i_IF+1}',conj(results.antIF(i_IF).weights));
                IF_imp = radiator(dirac,results.txAngle{i_IF+1}',conj(results.antIF(i_IF).weights));
            else
                radiator.release();
                IF = radiator(results.sig{1},results.txAngle{i_IF+1}');
                IF_imp = radiator(dirac,results.txAngle{i_IF+1}');
            end
            % Signal propagation including pathloss, delay, and reflection losses    
            IF = IF*diag(results.p_loss{i_IF+1});
            IF = delayseq(IF,results.delay_samp{i_IF+1});

            IF_imp = IF_imp*diag(results.p_loss{i_IF+1});
            IF_imp = delayseq(IF_imp,results.delay_samp{i_IF+1});

            phase_refl = phaseRefl(results.refAngle{i_IF+1});

            IF = IF*diag(results.refl_coef{i_IF+1}.*phase_refl);
            IF_imp = IF_imp*diag(results.refl_coef{i_IF+1}.*phase_refl);
            %Adding some noise
%             noisesig = sqrt(npower/2)*(randn(size(IF)) + 1i*randn(size(IF)));
%             IF = IF + noisesig;
            % RX Beamforming 
            if results.antRx.enableBeamforming
                collector.release();
                IF = collector(IF,results.rxAngle{i_IF+1}',conj(results.antRx.weights));
                IF = sum(IF,2);
                IF_imp = collector(IF_imp,results.rxAngle{i_IF+1}',conj(results.antRx.weights));
                IF_imp = sum(IF_imp,2);
                %imp_resp = sum(dirac,2);
            else
                collector.release();
                IF = collector(IF,results.rxAngle{i_IF+1}');
                IF = sum(IF,2);
                IF_imp = collector(IF_imp,results.rxAngle{1}');
                IF_imp = sum(IF_imp,2);
            end
            sumIF = sumIF+IF;
            sumIF_resp = sumIF_resp+IF_imp;
            IFPower = sum(abs(sumIF).^2)/length(sumIF);
            SNR = SPower/(IFPower+nPower);
        end
    else
        sumIF = [];
        sumIF_resp = [];
    end
end

