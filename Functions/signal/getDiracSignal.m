%GETDIRACSIGNAL gets a 1xlength(n) dirac function with n samples
%   
% 	Project: 		mmWave - Indoor simulation tool
% 	Author: 		Mathias Marscholl
% 	Affiliation:	SEEMOO, TU Darmstadt
% 	Date: 			22.03.2019
%   
%   Input:  
%       n: Sample Vector
%   Output: 
%    dirac: Diracfunction with the lenght of n
function dirac = getDiracSignal(n)
    dirac = zeros(length(n),1);
    dirac(1) = 1;
end

