%CANTENNA Class of an Antenna which contains the toolbox object, the 
%Position in the room and the angular direction of the mainlobe.
%
% 	Project:        mmWave - Indoor Simulation Tool
% 	Author:         Mathias Marscholl
% 	Affiliation:    SEEMOO, TU Darmstadt
% 	Date: 			08.10.2019
%
%   Properties:
%       type: String of the antennaType ('model','measurements',isotropic')
%       antObj: Toolbox object of the antenna
%       pos: position in the room
%       angle: angular direction of the mainlobe
%       enableBeamforming: Indicator if beamforming is enabled
%       weights: Antenna weights for the desired beamforming direction
%       steering_direction: The beamforming angular direction
%
%   Methods:
%       getBeamformingWeights: Gets the appropriate beamforming weights for a given
%       angle
%       plotPattern: Plots the efield pattern of the antenna with or
%       without beamforming
classdef cAntenna    
    properties
        type
        antObj
        pos
        direction
        enableBeamforming
        weights
        steering_direction
    end
    
    methods
        function obj = cAntenna(varargin)
            p=inputParser();
            checkString = @(s) any(strcmp(s,{'isotropic','model','measurements'}));
            p.addRequired('type',checkString);
            p.addRequired('pos', @isrow);
            p.addRequired('direction', @isvector);
            
            p.addParameter('enable_Beamforming', false, @islogical)
            p.parse(varargin{:});
            cfg = p.Results();
            obj.type = cfg.type;
            obj.antObj = getAntennaElements(obj.type);
            obj.pos = cfg.pos;
            obj.direction = wrapTo180(cfg.direction);
            obj.enableBeamforming = cfg.enable_Beamforming;
            if ~cfg.enable_Beamforming
                if isequal('isotropic',cfg.type)
                    obj.weights = 1;
                    obj.steering_direction = 0;
                else
                    obj.weights = ones(obj.antObj.getNumElements);
                    obj.steering_direction = 0;
                end
            else
                obj.weights = [];
                obj.steering_direction = [];
            end
        end
        function w = getBeamformingWeights(obj,steer_angle)
            if(obj.enableBeamforming == true)
                switch obj.type
                    case 'model'
                        steer = phased.SteeringVector('SensorArray',obj.antObj,'NumPhaseShifterBits',2,'IncludeElementResponse',false);
                        w = steer(60e9,steer_angle);
                    case 'measurements'
                        azang = obj.antObj.ElementSet{1,1}.AzimuthAngles;
                        w = get_weights_meas(steer_angle,azang);
                    case 'isotropic'
                        disp('Element is a single isotropic radiator. The steeringvector is 1');
                        w = 1;
                end
            else
                error('Beamforming is not activated. The object property ''enableBeamforming'' is false');
            end
            
        end
        function plotPattern(obj,steer_angle)
            if(strcmp(obj.type,'isotropic'))
                obj.antObj.pattern(60e9,[-180:180],0,'Type','efield','CoordinateSystem','rectangular','Normalize',false);
            elseif(obj.enableBeamforming)
                switch nargin
                    case 2
                        w = obj.getBeamformingWeights(steer_angle);
                        w = exp(1i*angle(w));
                        obj.antObj.pattern(60e9,[-180:180],0,'Type','efield','CoordinateSystem','rectangular','Normalize',false,'Weights',w);
                    case 1
                        obj.antObj.pattern(60e9,[-180:180],0,'Type','efield','CoordinateSystem','rectangular','Normalize',false);
                end
            else
                obj.antObj.pattern(60e9,[-180:180],0,'Type','efield','CoordinateSystem','rectangular','Normalize',false);
            end
        end
    end
end

