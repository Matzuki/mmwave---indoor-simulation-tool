%GETINTERFERENCERESPONSE Calculate the impact of other interfering 
% transmitting antennas. 
%   
% 	Project:        mmWave - Indoor Simulation Tool
% 	Author:         Mathias Marscholl
% 	Affiliation:    SEEMOO, TU Darmstadt
% 	Date: 			08.10.2019
%
%   Input:
%   antRx:          The current receiving antennas
%   antInt:         All interfering antennas
%   environment:    The given configuration
%   signal:         A general signal from all interferer
%
%   Output:
%   interference:   The received signal from all interferer
%   int_resp:       The impulse response from all interferer
function [interference,int_resp] = getInterferenceResponse(antRx,antInt,cfg)
    collector = phased.Collector('Sensor',antRx.antObj,'OperatingFrequency',cfg.fc,'WeightsInputPort',antRx.enableBeamforming);
    environment = getEnvironment(cfg.room,cfg.eps_wall,cfg.obstacles);
    interference = zeros(size(cfg.sig));
    int_resp = interference;
    
    for ii = 1:numel(antInt)
        dirac = getDiracSignal(cfg.sig);
        paths = tracePaths(antInt{ii}.pos, antRx.pos, environment, cfg.max_refl);
        intangle = relAngle(paths,antInt{ii}.direction,'TX');
        rxangle = relAngle(paths,antRx.direction,'RX');
        refangle = rad2deg(getPathReflectionAngles(paths));
        pathlength = getPathLength(paths);
        toa = pathlength/physconst('lightspeed');
        refl_coef = getReflectionCoeff(environment,paths,refangle);
        [delay_samp, p_loss] = losses_delay(toa,pathlength,cfg.f_sample);
        
        nFactor = max(antInt{ii}.antObj.pattern(60e9,-180:180,0,'Type','efield','CoordinateSystem','rectangular','Normalize',false));
%         nFactor = 1;
        if antInt{ii}.enableBeamforming
            radiator = phased.Radiator('Sensor',antInt{ii}.antObj,'OperatingFrequency',cfg.fc,'WeightsInputPort',antInt{ii}.enableBeamforming);
            sig = radiator(cfg.sig(:,ii),intangle',conj(antInt{ii}.weights))/nFactor;
            dirac = radiator(dirac,intangle',conj(antInt{ii}.weights))/nFactor;
        else
            radiator = phased.Radiator('Sensor',antInt{ii}.antObj,'OperatingFrequency',cfg.fc,'WeightsInputPort',antInt{ii}.enableBeamforming);
            sig = radiator(cfg.sig(:,ii),intangle')/nFactor;
            dirac = radiator(dirac,intangle')/nFactor;
        end
        rsig = sig*diag(p_loss);
        rsig = delayseq(rsig,delay_samp);
        dirac = dirac*diag(p_loss);
        dirac = delayseq(dirac,delay_samp);
        phase_refl = phaseRefl(refangle);
    
        rsig = rsig*diag(refl_coef.*phase_refl);
        dirac = dirac*diag(refl_coef.*phase_refl);
        
        nFactor = max(antRx.antObj.pattern(60e9,-180:180,0,'Type','efield','CoordinateSystem','rectangular','Normalize',false));
%         nFactor = 1;
        if antRx.enableBeamforming
            rsig = collector(rsig,rxangle',conj(antRx.weights))/nFactor;
            rsig = sum(rsig,2);
            dirac = collector(dirac,rxangle',conj(antRx.weights))/nFactor;
            dirac = sum(dirac,2);
            collector.release();
        else
            rsig = collector(rsig,rxangle')/nFactor;
            rsig = sum(rsig,2);
            dirac = collector(dirac,rxangle')/nFactor;
            dirac = sum(dirac,2);
            collector.release();
        end
        interference = interference + rsig;
        int_resp = int_resp + dirac;
    end
end

