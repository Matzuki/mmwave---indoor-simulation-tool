%GETBESTSECTOR Calculate the signal to interference ratio (SIR) for all 
% sectors
%   
% 	Project:        mmWave - Indoor Simulation Tool
% 	Author:         Mathias Marscholl
% 	Affiliation:    SEEMOO, TU Darmstadt
% 	Date: 			08.10.2019
%
%   Input:
%   antTx:          The current transmitting antennas
%   antRx:          The current receiving antennas
%   antInt:         All interfering antennas
%   environment:    The given configuration
%   signal:         A general signal from all interferer
%
%   Output:
%   interference:   The received signal from all interferer
%   int_resp:       The impulse response from all interferer
function [SIR,sectors] = getBestSector(antTx,antRx,antInt,cfg)
    sectors = transpose(-180:20:160);
    environment = getEnvironment(cfg.room,cfg.eps_wall,cfg.obstacles);
    SIR = zeros(length(sectors),numel(antInt)+1,1);
    
    paths = tracePaths(antTx.pos, antRx.pos, environment, cfg.max_refl);
    SIR(:,1) = getSectorPower(antTx,antRx,paths,environment,cfg.f_sample);
    for ii = 1:numel(antInt)
        paths = tracePaths(antInt{ii}.pos, antRx.pos, environment, cfg.max_refl);
        SIR(:,ii+1) = getSectorPower(antInt{ii},antRx,paths,environment,cfg.f_sample);
    end
    SIR = SIR(:,1)./(sum(SIR(:,2:end),2)+1e-6);
end

