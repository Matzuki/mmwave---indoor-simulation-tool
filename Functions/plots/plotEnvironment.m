%%
% Plots the environment with all paths and obstacles. 
%
% 	Project:        mmTrace - Real Antenna
% 	Author:         Mathias Marscholl
% 	Affiliation:    SEEMOO, TU Darmstadt
% 	Date:           15.04.2019
%   
%   Input:
%   traceset: The result struct from the tracing function
%   

function plotEnvironment(traceset)
    for ii=1:size(traceset,2)
%         figure
        figure('Renderer', 'painters', 'Position', [10 10 800 600])
        hold on;
        room_dim = [traceset(ii).environment(1,1)-traceset(ii).environment(1,3)/2, traceset(ii).environment(1,2)-traceset(ii).environment(1,4)/2, traceset(ii).environment(1,3), traceset(ii).environment(1,4)];
        rectangle('Position',room_dim);
        axis equal
        axis off
%         for iii=1:size(traceset,2)
%             drawPoint(traceset(iii).antTx.pos(1),traceset(iii).antTx.pos(2),'color','b','linewidth',2);
%             plotDirection(traceset(iii).antTx);
%             drawPoint(traceset(iii).antRx.pos(1),traceset(iii).antRx.pos(2),'color','r','linewidth',2);
%             plotDirection(traceset(iii).antRx);
%         end
        plotObstacles(traceset(ii).environment(2:end,:));
        cmap = autumn(3);
        plotPaths(traceset(ii).paths{1},traceset(ii).reflOrder{1},cmap);
%         title([num2str(ii),"constellation"])
        for iii=1:size(traceset,2)
            drawPoint(traceset(iii).antTx.pos(1),traceset(iii).antTx.pos(2),'color','b','linewidth',2);
            plotDirection(traceset(iii).antTx);
            drawPoint(traceset(iii).antRx.pos(1),traceset(iii).antRx.pos(2),'color','r','linewidth',2);
            plotDirection(traceset(iii).antRx);
        end
    end
end

function plotObstacles(obstacles)
    if(~isempty(obstacles))
        obstacles(:,5) = rad2deg(obstacles(:,5));
        drawOrientedBox(obstacles(:,1:5),'color','k','LineWidth',2);
    end
end

function plotDirection(antenna)
    x0 = antenna.pos(1);
    y0 = antenna.pos(2);
    drawArrow(x0,y0,x0+0.2*cosd(antenna.direction),y0+0.2*sind(antenna.direction),0.1,0.05,1);
    if(antenna.enableBeamforming)
        drawArrow(x0,y0,x0+0.5*cosd(antenna.direction+antenna.steering_direction),y0+0.5*sind(antenna.direction+antenna.steering_direction),0.1,0.05,1);
    end
end

function plotPaths(paths,refOrder,cmap)
    if(length(paths(1,:))==4)
        xvalues = [paths(1,1),paths(1,3)];
        yvalues = [paths(1,2),paths(1,4)];
        color = cmap(refOrder(1)+1,:);
        plot(xvalues,yvalues,'Color',color);
        if(~isempty(paths(2:end,:)))
            plotPaths(paths(2:end,:),refOrder(2:end,1),cmap);
        end
    else
        index = 3:5:length(paths(1,:));
        xvalues = [paths(1,1),paths(1,index)];
        yvalues = [paths(1,2),paths(1,index+1)];
        color = cmap(refOrder(1)+1,:);
%         plot(xvalues',yvalues','Color','magenta');
        plot(xvalues',yvalues','Color',color);
        if(~isempty(paths(2:end,:)))
            plotPaths(paths(2:end,:),refOrder(2:end,1),cmap);
        end
    end    
end