function drawSectors(traceset)
%%
% Plots the sectors with the corresponding SIR calculated by
% P_rec/(P_interf + noise)
% the noise level is definden by 1e-6;
%
% 	Project:       mmTrace - Real Antenna
% 	Author:        Mathias Marscholl
% 	Affiliation:     SEEMOO, TU Darmstadt
% 	Date: 			12.06.2019
%   
%   Input:
%   traceset: The result struct from the tracing function
%       
    for i_set=1:size(traceset,2)
        rxshift = traceset(i_set).antRx.direction;
        sectors = traceset(i_set).SIR(:,1);
        sectors = wrapTo180(sectors + rxshift);

        SIR = traceset(i_set).SIR(:,2);
        SIR = mag2db(SIR);
        cmap = jet(length(sectors));
        figure;
        axis([-1.3 1.3 -1.3 1.3]);
        hold on
        axis equal
        axis off;
        title(['constelation ',num2str(i_set)]);
        for ii=1:length(sectors)
            s = [0 0 1 sectors(ii), 20];
            drawEdge([0 0 cosd(sectors(ii)) sind(sectors(ii))],'LineWidth', 3, 'Color', cmap(ii,:)) ;

            drawCircleArc(s, 'LineWidth', 3, 'Color', cmap(ii,:))
            text(0.8*cosd(sectors(ii)+10),0.8*sind(sectors(ii)+10),num2str(SIR(ii),'%.2f'),'HorizontalAlignment','center');
            %if ii<length(sectors)
                text(1.2*cosd(sectors(ii)+10),1.2*sind(sectors(ii)+10),[num2str(sectors(ii)-rxshift),'� - ',num2str(sectors(ii)+20-rxshift),'�'],'HorizontalAlignment','center');
            %end
        end
    end
end

