function [border_rays] = visibleIntersect(pos,rays,environment)
% gets only the visible intersections
%   Input: 
%       pos: Position of the transmitter
%       rays: the given rays
%       environment: The current environment including room and obstacles
%   Output:
%       border_rays: only the visible rays which will be the borders for 
%                    the lit polygons       
    border_rays = [];
    poly = cell(size(environment,1),1);
    for ii = 1:size(environment,1)
        poly{ii} = orientedBoxToPolygon(environment(ii,:));
    end
    
    for i_rays = 1:size(rays,1)        
        intersect_points = [];
        for ii = 1:numel(poly)
            temp_intersect = intersectRayPolygon(rays(i_rays,:),poly{ii});
            temp_intersect = horzcat(temp_intersect,ii*ones(size(temp_intersect,1),1));
            intersect_points = [intersect_points;temp_intersect];
        end
        dist = distancePoints(pos,intersect_points(:,1:2));
        [~,minindex] = min(dist);
        pRay = round(intersect_points(minindex,1:2),8);
        pPoly = round(poly{intersect_points(minindex,3)},8);
        if ismember(pRay,pPoly)
            border_rays = [border_rays;rays(i_rays,:)];
        end
    end
end