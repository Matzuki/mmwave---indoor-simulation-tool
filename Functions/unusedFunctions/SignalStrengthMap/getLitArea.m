function [litPoly] = getLitArea(borderrays,environment)
% Gets all polygons which representing the lit area
%   Input: 
%       borderrays: all the rays which are intersecting all visible corners
%       environment: The current environment including room and obstacles
%   Output:
%       rays: All lit polygons as a cell array   
    litPoly = cell(size(borderrays,1),1);
    ii = 1;
    for i_rays = 1:size(borderrays,1)
        if(i_rays == size(borderrays,1))
            litPoly{ii} = getNearestIntersection(borderrays(i_rays,:),borderrays(1,:),environment);
        else
            litPoly{ii} = getNearestIntersection(borderrays(i_rays,:),borderrays(i_rays+1,:),environment);
            ii = ii+1;
        end
    end
end