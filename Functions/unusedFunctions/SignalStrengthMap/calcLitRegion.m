function [map] = calcLitRegion(X,Y,Z,posTx,environment)
% Calculate the lit region of the given setup
%   Input: 
%       X: Grid in x direction
%       Y: Grid in y direction
%       Z: The current signal strength map
%       posTx: Position of the transmitter
%       environment: The current environment including room and obstacles
%   Output:
%       map: The signal strength map respecting only the lit region       
    [rays] = getEdgeRays(posTx,environment);
    border_rays = visibleIntersect(posTx,rays,environment);
    litPoly = getLitArea(border_rays,environment);
    litRegion = zeros(size(X));
    
    for iipoly = 1:length(litPoly)
        tempPoly = litPoly{iipoly};
        in = inpolygon(X,Y,tempPoly(:,1)',tempPoly(:,2)');
        litRegion(in) = 1;
    end
    map = litRegion.*Z;
end