function [P] = getNearestIntersection(ray1,ray2,environment)
% gets only the neares intersection polygon between two rays and the given
% environment
%   Input: 
%       ray1: first(left) ray of the future polygon
%       ray2: second(right) ray of the future polygon
%       environment: The current environment including room and obstacles
%   Output:
%       P: the nearest visible polygon   
    pos = [ray1(1),ray1(2)];
    P1 = [];
    P2 = [];
    for ii = 1:size(environment,1)
        poly = orientedBoxToPolygon(environment(ii,:));
        tempP1 = intersectRayPolygon(ray1,poly);
        tempP1 = [tempP1,ii*ones(size(tempP1,1),1)];
        tempP2 = intersectRayPolygon(ray2,poly);
        tempP2 = [tempP2,ii*ones(size(tempP2,1),1)];
        if(~isempty(tempP1)&&~isempty(tempP2))
            P1 = [P1;tempP1];
            P2 = [P2;tempP2];
        end
    end
%     dist1 = distancePoints(pos,P1(:,1:2))';
    dist2 = distancePoints(pos,P2(:,1:2));
%     [~,index1] = min(dist1);
    [~,index2] = min(dist2);
    objID = P2(index2,3);
    
    A = P1(P1(:,3)==objID,1:2);
    dist1 = distancePoints(pos,A);
    [~,index1] = min(dist1);
    A = A(index1,:);
    B = P2(index2,1:2);
    P = [pos;A;B];
end