function [rays] = getEdgeRays(posTx,environment)
% Gets all rays which are intesecting corners of the environment
%   Input: 
%       posTx: Position of the transmitter
%       environment: The current environment including room and obstacles
%   Output:
%       rays: All rays which are intersecting corner of the objects   
    boxpoly = orientedBoxToPolygon(environment(1,:));
    angles = atan2((boxpoly(:,2)-posTx(2)),(boxpoly(:,1)-posTx(1)));
    rays = createRay(repmat(posTx,length(angles),1),angles);
    obstacles = environment(2:end,:);
    if(~isempty(obstacles))
        for ii=1:size(obstacles,1)
            poly = orientedBoxToPolygon(obstacles(ii,:));
            angles = atan2((poly(:,2)-posTx(2)),(poly(:,1)-posTx(1)));
            temprays = createRay(repmat(posTx,length(angles),1),angles);
            rays = [rays;temprays];
        end
    end
    angles = atan2d(rays(:,4),rays(:,3));
    [~,ind] = sort(wrapTo360(angles));
    angles = angles(ind);
    rays = rays(ind,:);
end
