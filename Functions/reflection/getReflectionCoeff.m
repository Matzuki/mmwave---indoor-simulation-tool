%GETREFLECTIONCOEFF Get the reflection coefficient 
%   from the different paths
%   
% 	Project: 		mmTrace - Real Antenna
% 	Author: 		Mathias Marscholl
% 	Affiliation:	SEEMOO, TU Darmstadt
% 	Date: 			25.03.2019
%
%   Input:
%   environment:    The environment room and all obstacles
%   paths:          All paths from the ray-tracing algorithm
%
%   Output:
%   refl_coeff: the combined reflection coefficient for each path
function [refl_coeff] = getReflectionCoeff(environment,paths,reflangle)
    n1 = 1;
    if(mod(size(paths,2),5)~=0)
        paths = paths(:,3:end-2);
    end
    objID = paths(:,5);
    objID(isnan(objID)) = 1;
    eps_r = environment(objID,6);

    [r_s, r_p] = fresnel(deg2rad(reflangle(:,1)), n1, sqrt(eps_r));
    temp_coeff = sqrt((r_p.^2 + r_s.^2)./2);
    temp_coeff(isnan(temp_coeff)) = 1;
    paths = paths(:,6:end);
    reflangle = reflangle(:,2:end);

    if(~isempty(paths))
        refl_coeff = [temp_coeff, getReflectionCoeff(environment,paths,reflangle)];
        refl_coeff = prod(refl_coeff,2);
    else
        refl_coeff = temp_coeff;
        refl_coeff = prod(refl_coeff,2);
    end
end

