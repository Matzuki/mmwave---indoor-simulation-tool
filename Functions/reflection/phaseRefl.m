%PHASEREFL Calculate the phase change due tu reflections
%
% 	Project: 		mmWave - Indoor simulation tool
% 	Author: 		Mathias Marscholl
% 	Affiliation:	SEEMOO, TU Darmstadt
% 	Date: 			19.10.2019
%   Input:
%   refangle: The reflection angle
%   
%   Output:
%   phase_refl: the phasechange for each path
function phase_refl = phaseRefl(refangle)
    phase_refl = zeros(size(refangle)) .* refangle;
    phase_refl(~isnan(phase_refl)) =  exp(1i*pi);
    phase_refl(isnan(phase_refl)) = nan+1i*nan;
    phase_refl = prod(phase_refl,2,'omitnan');
end